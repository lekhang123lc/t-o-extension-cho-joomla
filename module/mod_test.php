<?php

defined('_JEXEC') or die;

$db = JFactory::getDbo();
$query = $db->getQuery(true);
$query->select('id,catid,title');
$query->from('#__content');

$conditional="";
foreach( $params->get('catid') as $key => $catid ){
	if ( $key > 0 ) $conditional.=" or ";
	$conditional.="catid = $catid";
}
$query->where($conditional);
$db->setQuery((string)$query);
$res = $db->loadObjectList();
		
$count=1;
$link="";

foreach( $res as $row ){
	if ( $count > $params->get('count') ) break;
	$url = JRoute::_(ContentHelperRoute::getArticleRoute($row->id,  $row->catid));
	$link.="<a href=$url > $row->title </a> <br/>";
	$count++;
	}
echo $link;

?>